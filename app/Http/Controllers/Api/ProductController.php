<?php

/*
|--------------------------------------------------------------------------
| Product Controller
|--------------------------------------------------------------------------
|
| Product controller contains the APIs related to Product details, to get and store recently viewed products.
|
|
*/


namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Auth;
use Validator;
use App\Product;

class ProductController extends Controller
{
    

    /**
        @OA\Post(
            path="/products",tags={"Product"},summary="Products list",operationId="products",description= "Returns the success message and products list, Returns the error message if validation error or any other error occurred.",

			@OA\Response(response=200,description="Products list",
			@OA\MediaType(mediaType="application/json")),
			@OA\Response(response=401,description="Unauthenticated"),
        )
     */

    /*********** function for product list *********/
	public function index(Request $request)
	{

		$orderby = $request->get('orderby');
		$order = $request->get('order');

		$response = ['message'=> 'Products list','response' => Product::getProductModel($orderby, $order)];

     	return response($response, 200);
	}
}